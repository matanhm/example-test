<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('mytasks', 'TaskController@mytasks')->name('mytasks')->middleware('auth');
Route::resource('tasks', 'TaskController')->middleware('auth');
Auth::routes();
Route::get('/delete/{id}', 'TaskController@destroy')->name('delete');
Route::get('/statusupdate/{id}', 'TaskController@update_status')->name('statusupdate');
Route::get('/home', 'HomeController@index')->name('home');
