
<h1>This is Your Task List</h1>
<a href="{{route('tasks.index')}}">Show all tasks</a>
<ul>
    @foreach($tasks as $task)
    <li>
       id:{{$task->id}} title:{{$task->title}}  <a href= "{{route('tasks.edit', $task->id )}}">Edit</a>  @cannot('employee') <a href= "{{route('delete', $task->id )}}">Delete</a>@endcannot
    </li>
    @endforeach
</ul>