<h1>This is your Tasks list</h1>

<a href="{{route('mytasks')}}">My Tasks </a>

<ul>
    @foreach($tasks as $task)
    <li>
       id:{{$task->id}} title:{{$task->title}}  @cannot('employee') <a href= "{{route('tasks.edit', $task->id )}}">Edit</a> @endcannot
       @cannot('employee')<a href= "{{route('delete', $task->id )}}">Delete</a> @endcannot 
       @if($task->status!=1)
       @cannot('employee')  <a href= "{{route('statusupdate', $task->id )}}">Mark As Done </a> @endcannot
           @else
           <a>Done</a>
        @endif
    </li>
    @endforeach
</ul>

<a href="{{route('tasks.create')}}">Create a New Task </a>

