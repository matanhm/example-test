<h1>Edit a task</h1>
<form method = 'post' action="{{action('TaskController@update', $task->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">Task to Update:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$task->title}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Update Task">
</div>

</form>

